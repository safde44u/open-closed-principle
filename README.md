# Open Closed Principle

## Übung im `example` Package

In dieser Übung werden verschiedene Formen vom `DrawingService` gezeichnet. Dadurch müssen wir die
`draw(Shape s)` Methode ändern, sobald eine neue Form hinzu kommt. Dies verletzt das OCP.

Eine Implementierung die das OCP nicht verletzt ist, wenn jede Form für das Zeichnen ihrer Form selbst
verantwortlich ist. Das `DrawingService` kann grundsätzlich bleiben, allerdings sollte es nur mehr an
eine `draw()`-Methode der jeweiligen Form delegieren.

Durch dein Einsatz eines Interfaces lässt sich erzwingen, dass eine Form die `draw()`-Methode implementiert.

## Übung im `exercise` Package

In dieser Übung finden wir einen Rechner (`Calculator`), dem man in der `calculate()`-Methode eine `Operation`
übergeben kann. Anhand des konkreten Typs (aktuell `Addition` oder `Subtraction`) wird die entsprechende
Rechenoperation ausgeführt und in dem `result`-Property gespeichert.

Nun wollen wir eine weitere Operation hinzufügen, nämlich die Multiplikation. Überlege eine Lösung, sodass das
OCP nicht verletzt wird und ändere den (bestehenden) Code entsprechend.