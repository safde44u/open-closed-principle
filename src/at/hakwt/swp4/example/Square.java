package at.hakwt.swp4.example;

public class Square extends  Shape{

    public void draw()
    {
        System.out.println("Drawing a square");
        System.out.println(" ---- ");
        System.out.println(" |  | ");
        System.out.println(" ---- ");
    }
}
