package at.hakwt.swp4.example;

public class OpenClosedPrincipleMain {

    public static void main(String[] args) {
        DrawingService drawingService = new DrawingService();
	    Shape square = new Square();
        drawingService.draw(square);

        Shape circle = new Circle();
        drawingService.draw(circle);

        Shape line = new Line();
        drawingService.draw(line);

    }
}
