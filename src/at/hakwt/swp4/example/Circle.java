package at.hakwt.swp4.example;

public class Circle extends Shape
{

    public void draw()
    {
        System.out.println("Drawing a circle");
        System.out.println("  _   ");
        System.out.println(" / \\ ");
        System.out.println(" \\ / ");
        System.out.println("  -   ");
    }
}
