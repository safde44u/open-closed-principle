package at.hakwt.swp4.exercise;

public class CalculatorMain {

    public static void main(String[] args) {
        Calculator c = new Calculator();

        Addition a = new Addition(10, 10);
        c.calculate(a);
        System.out.println(a.getLeft() + " + " + a.getRight() + " = " + a.getResult());

        Subtraction s = new Subtraction(10, 5);
        c.calculate(s);
        System.out.println(s.getLeft() + " - " + s.getRight() + " = " + s.getResult());
    }

}
